void* array_mass_push(
	struct array* this,
	unsigned long index,
	const void* ele,
	unsigned long n);
