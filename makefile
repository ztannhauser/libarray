

CC = gcc
CPPFLAGS = -I . -I ..
CFLAGS += -Wall -Werror=implicit-function-declaration
# LDLIBS = -lm

TARGET = libarray

default: $(TARGET).a

srclist.mk:
	find -name '*.c' ! -path './#*' | sed 's/^/SRCS += /' > srclist.mk

include srclist.mk

OBJS = $(SRCS:.c=.o)
DEPENDS = $(SRCS:.c=.mk)

install: ../$(TARGET).a ../$(TARGET).h

run: $(TARGET)
	./$(TARGET) $(ARGS)

valrun: $(TARGET)
	valgrind ./$(TARGET) $(ARGS)

../$(TARGET).a: $(TARGET).a
	cp $(TARGET).a ../

../$(TARGET).h: $(TARGET).h
	cp $(TARGET).h ../

target_h_deps += struct.h
target_h_deps += clear.h
target_h_deps += clone.h
target_h_deps += delete.h
target_h_deps += foreach.h
target_h_deps += mass_push_n.h
target_h_deps += new.h
target_h_deps += pop_n.h
target_h_deps += push_n.h
target_h_deps += qsort.h

$(TARGET).h: $(target_h_deps)
	echo "#ifndef" $(TARGET)"_H" > $(TARGET).h
	echo "#define" $(TARGET)"_H" >> $(TARGET).h
	cat $(target_h_deps) >> $(TARGET).h
	echo "#endif" >> $(TARGET).h

$(TARGET).a: $(OBJS)
	$(LD) -r $^ -o $@

%.l.mk: %.l
	echo "$(basename $<).c: $<" > $@

%.y.mk: %.y
	echo "$(basename $<).c: $<" > $@

%.mk: %.c
	$(CPP) -MM -MT $@ $(CPPFLAGS) -MF $@ $<

%.o: %.c %.mk
	$(CC) -c $(CPPFLAGS) $(CFLAGS) $< -o $@

.PHONY: clean deep-clean

clean:
	rm $(OBJS) $(DEPENDS) srclist.mk $(TARGET).{a,h}

deep-clean:
	find -type f -regex '.*\.mk' -delete
	find -type f -regex '.*\.o' -delete
	find -type f -executable -delete

include $(DEPENDS)

