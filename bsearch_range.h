void* array_bsearch_range(
	struct array* this,
	void* min, void* max,
	int (*compar)(const void*, const void *),
	uintmax_t* n
);
