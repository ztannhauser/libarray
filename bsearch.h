
void* array_bsearch(
	struct array* this,
	void* ele,
	int (*compar)(const void *, const void *)
);
