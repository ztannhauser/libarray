
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "struct.h"

struct array* array_init(struct array* this, int elesize)
{
	this->n = 0;
	this->data = NULL;
	this->mem_length = 0;
	this->elesize = elesize;
	return this;
}

struct array* array_init_given_data(
	struct array* this,
	char* data, unsigned long n,
	int elesize)
{
	this->n = n;
	this->mem_length = n * elesize;
	this->data = memcpy(malloc(this->mem_length), data, this->mem_length);
	this->elesize = elesize;
	return this;
}
