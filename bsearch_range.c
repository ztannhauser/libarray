
#include <stdlib.h>
#include <assert.h>

#include "struct.h"
#include "bfindplace.h"

void* array_bsearch_range(
	struct array* this,
	void* min, void* max,
	int (*compar)(const void*, const void *),
	uintmax_t* n
)
{
	void* ret;
	
	struct bfindplace_return start = array_bfindplace(
		this, NULL, ({
			int my_compar(const void* unused, const void* b)
			{
				int ret;
				int c = compar(min, b);
				if(c < 0) ret = c;
				else if(c > 0) ret = c;
				else
				{
					if(this->data < b)
					{
						c = compar(min, b - this->elesize);
						if(c < 0) abort(); // not sane
						else if(c > 0) ret = 0;
						else ret = -1;
					}
					else
					{
						ret = c;
					}
				}
				return ret;
			}
			my_compar;
		}));
	
	void* last = arrayp_index(*this, this->n - 1);
	
	struct bfindplace_return end = array_bfindplace(
		this, NULL, ({
			int my_compar(const void* unused, const void* b)
			{
				int ret;
				int c = compar(max, b);
				if(c < 0) ret = c;
				else if(c > 0) ret = c;
				else
				{
					if(b < last)
					{
						c = compar(max, b + this->elesize);
						if(c > 0) abort(); // not sane
						else if(c < 0) ret = 0;
						else ret = 1;
					}
					else
					{
						ret = c;
					}
				}
				return ret;
			}
			my_compar;
		}));
	
	*n = end.index - start.index;
	return arrayp_index(*this, start.index);
}


























