
#include <assert.h>
#include <stdlib.h>

#include "struct.h"

void array_mass_pop_n(struct array* a, unsigned long n)
{
	assert(a->n >= n);
	a->n -= n;
}
