#include <stdlib.h>

#include "struct.h"

void delete_array(struct array* a)
{
	free(a->data);
}
