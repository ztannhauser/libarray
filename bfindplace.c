#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>

#include "struct.h"

#include "bfindplace.h"

struct bfindplace_return array_bfindplace(
	struct array* this,
	void* ele,
	int (*compar)(const void *, const void *))
{
	struct bfindplace_return ret = {.exists = false};
	if(this->n)
	{
		uintmax_t start = 0, end = this->n - 1;
		while(start < end && !ret.exists)
		{
			int middle = (end + start) / 2;
			int c = compar(ele, arrayp_index(*this, middle));
			if(c < 0)
			{
				end = middle - 1;
			}
			else if(c > 0)
			{
				start = middle + 1;
			}
			else
			{
				ret.index = middle, ret.exists = true;
			}
		}
		if(!ret.exists)
		{
			int c = compar(ele, arrayp_index(*this, start));
			if(c > 0)
			{
				ret.index = start + 1, ret.exists = false;
			}
			else
			{
				ret.index = start, ret.exists = (c == 0);
			}
		}
	}
	else
	{
		ret.exists = false, ret.index = 0;
	}
	return ret;
}
