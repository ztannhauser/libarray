#include <stdlib.h>

#include "struct.h"

void array_foreach(struct array* this, void (*callback) (void* ele))
{
	for(int i = 0, n = this->n;i < n;i++)
	{
		void* ele = arrayp_index(*this, i);
		callback(ele);
	}
}

void arrayp_foreach(struct array* this, void (*callback) (void* ele))
{
	for(int i = 0, n = this->n;i < n;i++)
	{
		void* ele = array_index(*this, i, void*);
		callback(ele);
	}
}
