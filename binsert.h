
int array_binsert(
	struct array* this,
	void* ele,
	int (*compar)(const void *, const void *));
