#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include "struct.h"

void array_mass_pop(struct array* this, size_t index, size_t length)
{
	assert(this->n >= index + length);
	memmove(
		this->data + this->elesize * index,
		this->data + this->elesize * (index + length),
		this->elesize * (this->n - length - index)
	);
	this->n -= length;
}
