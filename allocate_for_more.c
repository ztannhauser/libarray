
#include <stdio.h>
#include <stdlib.h>

#include "struct.h"

void array_allocate_for_more(struct array* this, size_t m)
{
	while((this->n + m) * (this->elesize) > (this->mem_length))
	{
		this->mem_length = this->mem_length * 2 + 1;
		this->data = realloc(this->data, this->mem_length);
	}
}

