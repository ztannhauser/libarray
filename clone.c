
#include <string.h>
#include <stdlib.h>

#include "struct.h"

struct array array_clone(struct array* t)
{
	struct array ret;
	ret.n = t->n;
	ret.mem_length = t->mem_length;
	ret.elesize = t->elesize;
	ret.data = memcpy(malloc(t->mem_length), t->data, t->mem_length);
	return ret;
}
