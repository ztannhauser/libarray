#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include "struct.h"

void array_mass_pop_0(struct array* this, unsigned long length)
{
	assert(this->n >= length);
	memmove(
		this->data,
		this->data + this->elesize * length,
		this->elesize * (this->n - length)
	);
	this->n -= length;
}
