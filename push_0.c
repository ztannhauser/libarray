
#include <assert.h>
#include <string.h>

#include "struct.h"
#include "allocate_for_more.h"

void array_push_0(struct array* this, void* ele)
{
	array_allocate_for_more(this, 1);
	void* dest = this->data;
	memmove(dest + this->elesize, dest, this->n * this->elesize);
	if(ele)
	{
		memcpy(dest, ele, this->elesize);
	}
	this->n++;
	return dest;
}
