#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include "struct.h"
#include "lfind.h"

uintmax_t array_replace(struct array* this, void* element, void* replacement)
{
	#if 0
	size_t index = array_find(this, element);
	assert(index != (size_t) -1);
	memcpy(
		arrayp_index(*this, index),
		replacement,
		this->elesize);
	return index;
	#endif
	abort();
}
