#include <stdlib.h>
#include <string.h>

#include "struct.h"

void* array_lfind(struct array* this, void* element)
{
	#if 0
	for(size_t i = 0, n = this->n;i < n;i++)
	{
		void* ele = arrayp_index(*this, i);
		if(!memcmp(ele, element, this->elesize))
		{
			return i;
		}
	}
	return -1;
	#endif
	abort();
}
