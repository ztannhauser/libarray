
#include <stdio.h>

#include "struct.h"
#include "init.h"

struct array new_array_given_size(int elesize)
{
	struct array this;
	array_init(&this, elesize);
	return this;
}

struct array new_array_given_size_and_data(
	const char* data, unsigned long n,
	int elesize)
{
	struct array this;
	array_init_given_data(&this, data, n, elesize);
	return this;
}



