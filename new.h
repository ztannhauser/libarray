

#define new_arrayp() new_array(void*)
#define new_array(type) new_array_given_size(sizeof(type))
#define new_array_from_data(ptr,len,type) \
	new_array_given_size_and_data(ptr,len,sizeof(type))

struct array new_array_given_size(int elesize);


struct array new_array_given_size_and_data(
	const char* data, unsigned long n,
	int elesize);
