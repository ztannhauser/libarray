
#include <stdlib.h>

#include "./struct.h"

void array_qsort(struct array* this, int (*compar)(const void *, const void *))
{
	qsort(this->data, this->n, this->elesize, compar);
}

