#ifndef libarray_H
#define libarray_H

#include <inttypes.h>

struct array
{
	union {
		unsigned char *data;
		signed char *datas;
		char *datac;
		void *datavp;
		void **datavpp;
	};
	uintmax_t n;
	uintmax_t elesize;
	uintmax_t mem_length;
};

#define arrayptr_index(array, index)\
	array_index(array, index, void*)

#define array_index(array, index, datatype)\
	(((datatype *) ((array).data))[index])

#define arrayp_index(array, index)\
	((array).data + (index) * (array).elesize)
void array_clear(struct array* a);
struct array array_clone(struct array* a);

void delete_array(struct array* a);

void array_foreach(struct array* this, void (*callback) (void* ele));

void arrayp_foreach(struct array* this, void (*callback) (void* ele));
void* array_mass_push_n(
	struct array * this,
	const void *ele,
	unsigned long n);


#define new_arrayp() new_array(void*)
#define new_array(type) new_array_given_size(sizeof(type))
#define new_array_from_data(ptr,len,type) \
	new_array_given_size_and_data(ptr,len,sizeof(type))

struct array new_array_given_size(int elesize);


struct array new_array_given_size_and_data(
	const char* data, unsigned long n,
	int elesize);
void array_pop_n(struct array* a);
void* array_push_n(struct array* this, const void* ele);
void array_qsort(struct array* this, int (*compar)(const void *, const void *));
#endif
