#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>

#include "struct.h"
#include "bfindplace.h"
#include "push.h"

uintmax_t array_binsert(
	struct array* this,
	void* ele,
	int (*compar)(const void *, const void *))
{
	struct bfindplace_return place =
		array_bfindplace(this, ele, compar);
	array_push(this, place.index, ele);
	return place.index;
}
