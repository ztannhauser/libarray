
#include <stdbool.h>

struct bfindplace_return
{
	uintmax_t index;
	bool exists;
};

struct bfindplace_return array_bfindplace(
	struct array* this,
	void* ele,
	int (*compar)(const void *, const void *));
