
#include <assert.h>
#include <string.h>

#include "struct.h"

void array_pop_n(struct array* a)
{
	assert(a->n);
	a->n--;
}
