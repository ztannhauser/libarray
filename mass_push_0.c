

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "struct.h"
#include "allocate_for_more.h"

void array_mass_push_0(
	struct array* this,
	const void *ele,
	unsigned long n)
{
	array_allocate_for_more(this, n);
	void* dest = this->data;
	memmove(dest + n * this->elesize, dest, this->n * this->elesize);
	if(ele)
	{
		memcpy(dest, ele, n * this->elesize);
	}
	this->n += n;
	return dest;
}
