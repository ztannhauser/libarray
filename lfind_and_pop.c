#include <stdlib.h>

#include "struct.h"
#include "lfind.h"
#include "pop.h"

void* array_lfind_and_pop(struct array* this, void* element)
{
	#if 0
	size_t index = array_find(this, element);
	if(index != (size_t) -1)
	{
		array_pop(this, index);
	}
	return index;
	#endif
	abort();
}
