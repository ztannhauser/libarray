
#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "struct.h"
#include "allocate_for_more.h"

void* array_mass_push(
	struct array* this,
	unsigned long index,
	const void* ele,
	unsigned long n)
{
	assert(0 <= index && index <= this->n);
	array_allocate_for_more(this, n);
	void* dest = this->data + index * this->elesize;
	memmove(dest + this->elesize, dest, (this->n - index) * this->elesize);
	if(ele)
	{
		memcpy(dest, ele, this->elesize);
	}
	this->n += n;
	return dest;
}
