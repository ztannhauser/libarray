#include <stdlib.h>

#include "struct.h"

void* array_bsearch(
	struct array* this,
	void* ele,
	int (*compar)(const void *, const void *)
)
{
	return bsearch(ele, this->data, this->n, this->elesize, compar);
}
