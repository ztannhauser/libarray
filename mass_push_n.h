void* array_mass_push_n(
	struct array * this,
	const void *ele,
	unsigned long n);
